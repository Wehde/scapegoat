import db from "../config/database.js";


// Get List of ALL Users by Username
export const getUsernames = (result) => {
    db.query("SELECT username FROM users", (err, results) => {
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });
}

// Get One User
export const getUserById = (id, result) => {
    db.query("SELECT * FROM users WHERE id = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results[0]);
        }
    });
}

export const findUserByEmail = (id, result) => {
    db.query("SELECT * FROM users WHERE email = ?", [id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            console.log(id);
            result(null, results[0]);
        }
    });
}

// Register New User
export const insertUser = (data, result) => {
    db.query("INSERT INTO users SET ?", [data], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}

// Update User Streaming Sites
export const updateUserSitesById = (data, id, result) => {
    db.query("UPDATE users SET netflix = ?, amazon = ?, disney = ?, hbo = ?, hulu = ?, peacock = ?, paramount = ?, starz = ?, showtime = ?, apple = ? WHERE id = ?", [data.netflix, data.amazon, data.disney, data.hbo, data.hulu, data.peacock, data.paramount, data.starz, data.showtime, data.apple, id], (err, results) => {             
        if(err) {
            console.log(err);
            result(err, null);
        } else {
            result(null, results);
        }
    });   
}