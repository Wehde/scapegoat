import mysql from "mysql2";
import * as dotenv from 'dotenv'
dotenv.config()
   
// create the connection to database
const db = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
});

// create the table if it doesn't exist.
db.connect(function(err) {
    if (err) throw err;
    db.query("CREATE TABLE IF NOT EXISTS users("+
        "id INT PRIMARY KEY AUTO_INCREMENT,"+
        "username VARCHAR(50) NOT NULL,"+
        "email VARCHAR(100) NOT NULL,"+
        "password VARCHAR(255) NOT NULL,"+
        "netflix BOOLEAN NOT NULL,"+
        "amazon BOOLEAN NOT NULL,"+
        "disney BOOLEAN NOT NULL,"+
        "hbo BOOLEAN NOT NULL,"+
        "hulu BOOLEAN NOT NULL,"+
        "peacock BOOLEAN NOT NULL,"+
        "paramount BOOLEAN NOT NULL,"+
        "starz BOOLEAN NOT NULL,"+
        "showtime BOOLEAN NOT NULL,"+
        "apple BOOLEAN NOT NULL)"
    )
});
  
export default db;