// import express
import express from "express";
// import cors
import cors from "cors";
// import routes
import Router from "./routes/routes.js";

import path from "path";
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);
 
// init express
const app = express();
 
// use express json
app.use(express.json());

app.use(express.static(path.join(__dirname, '../frontend/dist')));
 
// use cors
app.use(cors());
 
// use router
app.use(Router);

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, '../frontend/dist', 'index.html'))
})
 
app.listen(process.env.PORT, () => console.log('Server running at https://portfolios.talentsprint.com/scapegoat/'));