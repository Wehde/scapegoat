import { getUsernames, getUserById, insertUser, updateUserSitesById, findUserByEmail } from "../models/userModel.js"

// Get List of ALL Users by Username
export const showUsernames = (req, res) => {
    getUsernames((err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Get One User
export const showUserById = (req, res) => {
    getUserById(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}

// Find User by Email
export const authUser = (req, res) => {
    console.log("TEST");
    findUserByEmail(req.params.id, (err, results) => {
        if (err){
            res.send(err);
        } else {
            res.json(results);
        }
    })
}

// Register New User
export const registerUser = (req, res) => {
    const data = req.body;
    insertUser(data, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}
 
// Update User Streaming Sites
export const updateUser = (req, res) => {
    const data  = req.body;
    const id    = req.params.id;
    updateUserSitesById(data, id, (err, results) => {
        if (err){
            res.send(err);
        }else{
            res.json(results);
        }
    });
}