import express from "express";

import { showUsernames, showUserById, registerUser, updateUser, authUser } from "../controllers/user.js";

const router = express.Router();

// Get List of ALL Users by Username
router.get('/users', showUsernames);

// Get One User
router.get('/users/:id', showUserById);

//Find User by Email
router.get('/auth/:id', authUser);

// Register New User
router.post('/users', registerUser);

// Update User Streaming Sites
router.put('/users/:id', updateUser);

export default router;